package pl.mrz.typed

import java.net._

import akka.actor.typed.scaladsl.adapter._
import akka.actor.{ActorContext, ActorRef, FSM}
import akka.pattern.ask
import akka.util
import pl.mrz.http.PaymentType.PaymentType
import pl.mrz.http.ProductCatalog.{GetItems, Items}
import pl.mrz.typed.CartManager.{CheckoutStarted, ItemAdded}
import pl.mrz.typed.CheckoutManager.{PaymentServiceStarted, SelectDelivery, SelectPayment}
import pl.mrz.typed.OrderManager._
import pl.mrz.typed.PaymentManager.{DoPayment, PaymentConfirmed, PaymentFailed}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Random

class OrderManager(id: String) extends FSM[State, Data] {
  startWith(Uninitialized, Empty())

  private implicit val timeout: util.Timeout = 5 seconds

  private def getProductFromCatalog(item: Item, context: ActorContext): Item = {
    val productCatalogSelection = context.actorSelection("akka.tcp://ProductCatalog@127.0.0.1:2554/user/productcatalog")
    val productCatalog = Await.result(productCatalogSelection.resolveOne(), 5 seconds)
    Item.fromItems(Await.result(productCatalog ? GetItems(item.brand, List(item.name)), 5 seconds).asInstanceOf[Items])
  }

  when(Uninitialized) {
    case x @ Event(AddItem(_) | RemoveItem(_, _), Empty()) =>
      val event = x.event match {
        case AddItem(item) => CartManager.AddItem(getProductFromCatalog(item, context), context.self)
        case RemoveItem(item, count) => CartManager.RemoveItem(item, count, context.self)
      }
      val cartId = Random.nextInt().toString
      val cart = context.spawn(CartManager.behavior(cartId), s"CartManager$cartId")
      cart ! event
      stay using CartDataWithSender(cart, sender)
    case Event(ItemAdded(_), CartDataWithSender(cart, client)) =>
      client ! Done
      goto(Open) using CartData(cart)
  }

  when(Open) {
    case x @ Event(AddItem(_) | RemoveItem(_, _), CartData(cart)) =>
      val event = x.event match {
        case AddItem(item) => CartManager.AddItem(getProductFromCatalog(item, context), context.self)
        case RemoveItem(item, count) => CartManager.RemoveItem(item, count, context.self)
      }
      cart ! event
      stay using CartDataWithSender(cart, sender)
    case Event(ItemAdded(_), CartDataWithSender(cart, client)) =>
      client ! Done
      stay using CartData(cart)
    case Event(Buy, CartData(cart)) =>
      cart ! CartManager.Buy(context.self)
      stay using CartDataWithSender(cart, sender)
    case Event(CheckoutStarted(checkout), CartDataWithSender(_, client)) =>
      client ! Done
      goto(InCheckout) using InCheckoutData(checkout.toUntyped)
  }

  when(InCheckout) {
    case Event(SelectDeliveryAndPaymentMethod(delivery, payment), InCheckoutData(checkout)) =>
      checkout ! SelectDelivery(delivery, context.self)
      checkout ! SelectPayment(payment, context.self)
      stay using InCheckoutDataWithSender(checkout, sender)
    case Event(PaymentServiceStarted(payment), InCheckoutDataWithSender(_, client)) =>
      client ! Done
      goto(InPayment) using InPaymentData(payment)
  }

  when(InPayment) {
    case Event(Pay(method), InPaymentData(payment)) =>
      payment ! DoPayment(method)
      stay using InPaymentDataWithSender(payment, sender)
    case Event(PaymentConfirmed, InPaymentDataWithSender(_, client)) =>
      client ! Done
      goto(Finished) using Empty()
    case Event(PaymentFailed, InPaymentDataWithSender(_, client)) =>
      client ! Failed
      goto(Finished) using Empty()
  }

  when(Finished) {
    case _ => stay
  }

  initialize()
}

object OrderManager {
  sealed trait State
  case object Uninitialized extends State
  case object Open          extends State
  case object InCheckout    extends State
  case object InPayment     extends State
  case object Finished      extends State

  sealed trait Command
  case class AddItem(item: Item)                                               extends Command
  case class RemoveItem(id: URI, count: Int)                                   extends Command
  case class SelectDeliveryAndPaymentMethod(delivery: String, payment: String) extends Command
  case object Buy                                                              extends Command
  case class Pay(method: PaymentType)                                          extends Command

  sealed trait Ack
  case object Done extends Ack //trivial ACK
  case object Failed extends Ack

  sealed trait Data
  case class Empty()                                                           extends Data
  case class CartData(cartRef: akka.actor.typed.ActorRef[CartManager.CartCommand[CartManager.CartEvent]])
                                                                               extends Data
  case class CartDataWithSender(cartRef: akka.actor.typed.ActorRef[CartManager.CartCommand[CartManager.CartEvent]],
                                sender: ActorRef)                              extends Data
  case class InCheckoutData(checkoutRef: ActorRef)                             extends Data
  case class InCheckoutDataWithSender(checkoutRef: ActorRef, sender: ActorRef) extends Data
  case class InPaymentData(paymentRef: ActorRef)                               extends Data
  case class InPaymentDataWithSender(paymentRef: ActorRef, sender: ActorRef)   extends Data
}