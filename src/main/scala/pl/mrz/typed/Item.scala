package pl.mrz.typed

import java.net.URI

import pl.mrz.http.ProductCatalog.Items

/**
  * @param id: unique item identifier (java.net.URI)
  */
case class Item(id: URI, name: String, brand: String, price: BigDecimal, count: Int)

object Item {
  def fromItems(items: Items): Item = {
    val firstItem = items.items.head
    Item(firstItem.id, firstItem.name, firstItem.brand, firstItem.price, firstItem.count)
  }
}