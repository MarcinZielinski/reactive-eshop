package pl.mrz.typed

import java.util.concurrent.CountDownLatch

import akka.actor.Props
import akka.actor.typed.scaladsl.adapter._
import akka.actor.typed.scaladsl.{ActorContext, Behaviors, TimerScheduler}
import akka.actor.typed.{ActorRef, Behavior}
import akka.persistence.typed.scaladsl.PersistentBehavior.EventHandler
import akka.persistence.typed.scaladsl.{Effect, PersistentBehavior, ReplyEffect}
import akka.persistence.typed.{ExpectingReply, PersistenceId}
import pl.mrz.typed.CartManager.{CartCommand, CartEvent}

import scala.concurrent.duration._

object CheckoutManager {

  type CheckoutStateCommandToReplyEffect = (CheckoutState, CheckoutCommand[CheckoutEvent]) => ReplyEffect[CheckoutEvent, CheckoutState]

  val selectingDelivery: CheckoutStateCommandToReplyEffect = // checkoutexpirationtime here
    (_, command) => command match {
      case CancelCheckout(_) =>
        val event = CheckoutCancelled()
        Effect.persist(event).thenReply(command)(_ => event)
      case CheckoutTimeout(_) =>
        Effect.persist(CheckoutTimerExpired()).thenNoReply()
      case SelectDelivery(method, _) =>
        Effect.persist(DeliverySelected(method)).thenNoReply()
      case _ => Effect.noReply
    }

  def selectingPayment(context: ActorContext[CheckoutCommand[CheckoutEvent]]): CheckoutStateCommandToReplyEffect = // checkout expiration time
    (_, command) => command match {
      case CancelCheckout(_) =>
        Effect.persist(CheckoutCancelled()).thenNoReply()
      case CheckoutTimeout(_) =>
        Effect.persist(CheckoutTimerExpired()).thenNoReply()
      case SelectPayment(_, _) =>
        val event = PaymentServiceStarted(context.actorOf(Props(new PaymentManager(context.self.toUntyped))))
        Effect.persist(event).thenReply(command)(_ => event)
      case _ => Effect.noReply
    }

  def processingPayment(context: ActorContext[CheckoutCommand[CheckoutEvent]], cart: ActorRef[CartCommand[CartEvent]]): CheckoutStateCommandToReplyEffect =  // payment epiration time
    (_, command) => command match {
      case CancelCheckout(_) =>
        val event = CheckoutCancelled()
        Effect.persist(event).thenNoReply()
      case PaymentTimeout(_) =>
        Effect.persist(PaymentTimerExpired()).thenNoReply()
      case PaymentReceivedCommand(_) =>
        val command: CartCommand[CartEvent] = CartManager.CloseCheckout(context.self.toUntyped)
        Effect.persist(PaymentReceived()).thenRun((state: CheckoutState) => cart ! command).thenNoReply()
      case _ => Effect.noReply
    }

  def commandHandler(context: ActorContext[CheckoutCommand[CheckoutEvent]], cart: ActorRef[CartCommand[CartEvent]]): CheckoutStateCommandToReplyEffect =
    (state, command) => state match {
      case _: SelectingDelivery => selectingDelivery(state, command)
      case _: SelectingPayment => selectingPayment(context)(state, command)
      case _: ProcessingPayment => processingPayment(context, cart)(state, command)
      case _ => Effect.noReply
    }

  def eventHandler(context: ActorContext[CheckoutCommand[CheckoutEvent]], timer: TimerScheduler[CheckoutCommand[CheckoutEvent]], paymentExpirationTime: FiniteDuration): EventHandler[CheckoutState, CheckoutEvent] =
    (state, event) => (event, state) match {
        case (CheckoutCancelled(), _) => Closed()
        case (CheckoutTimerExpired(), _) => Closed()
        case (PaymentTimerExpired(), _) => Closed()
        case (DeliverySelected(delivery), SelectingDelivery()) => SelectingPayment(delivery)
        case (PaymentServiceStarted(_), SelectingPayment(_)) =>
          timer.cancel(CheckoutTimer)
          timer.startSingleTimer(PaymentTimer, PaymentTimeout(context.spawnAnonymous(Behavior.empty)), paymentExpirationTime)
          ProcessingPayment()
        case (PaymentReceived(), ProcessingPayment()) =>
          timer.cancel(PaymentTimer)
          Closed()
        case _ => state
      }


  def behavior(checkoutId: String, cart: ActorRef[CartCommand[CartEvent]], checkoutExpirationTime: FiniteDuration = 1 second,
               paymentExpirationTime: FiniteDuration = 1 second, onRecoveryCompleteUnlockedLatch: Option[CountDownLatch] = None): Behavior[CheckoutCommand[CheckoutEvent]] =
    Behaviors.withTimers(timer =>
      Behaviors.setup(context => {
        timer.startSingleTimer(CheckoutTimer, CheckoutTimeout(context.spawnAnonymous(Behaviors.empty)), checkoutExpirationTime)
          PersistentBehavior.withEnforcedReplies(
          persistenceId = PersistenceId("CheckoutManager"),
          emptyState = SelectingDelivery(),
          commandHandler = commandHandler(context, cart),
          eventHandler = eventHandler(context, timer, paymentExpirationTime)
        )}.onRecoveryCompleted(_ => onRecoveryCompleteUnlockedLatch.foreach(_.countDown()))
      ))

  sealed trait CheckoutData
  sealed trait CheckoutState

  case class CheckoutParameters(delivery: String = "", payment: String = "") extends CheckoutData

  sealed trait CheckoutEvent
  case class PaymentServiceStarted(payment: akka.actor.ActorRef) extends CheckoutEvent
  case class DeliverySelected(deliveryMethod: String) extends CheckoutEvent
  case class CheckoutCancelled() extends CheckoutEvent
  case class CheckoutTimerExpired() extends CheckoutEvent
  case class PaymentTimerExpired() extends CheckoutEvent
  case class PaymentReceived() extends CheckoutEvent


  sealed trait CheckoutCommand[Event] extends ExpectingReply[Event]
  case class SelectDelivery(deliveryMethodId: String, override val replyTo: ActorRef[CheckoutEvent]) extends CheckoutCommand[CheckoutEvent]
  case class SelectPayment(paymentId: String, override val replyTo: ActorRef[CheckoutEvent]) extends CheckoutCommand[CheckoutEvent]
  case class CancelCheckout(override val replyTo: ActorRef[CheckoutEvent]) extends CheckoutCommand[CheckoutEvent]
  case class CheckoutTimeout(override val replyTo: ActorRef[CheckoutEvent]) extends CheckoutCommand[CheckoutEvent]
  case class PaymentTimeout(override val replyTo: ActorRef[CheckoutEvent]) extends CheckoutCommand[CheckoutEvent]
  case class PaymentReceivedCommand(override val replyTo: ActorRef[CheckoutEvent]) extends CheckoutCommand[CheckoutEvent]

  case class SelectingDelivery() extends CheckoutState
  case class SelectingPayment(deliveryMethod: String) extends CheckoutState
  case class ProcessingPayment() extends CheckoutState
  case class Canceled() extends CheckoutState
  case class Closed() extends CheckoutState

  case object CheckoutTimer
  case object PaymentTimer

}
