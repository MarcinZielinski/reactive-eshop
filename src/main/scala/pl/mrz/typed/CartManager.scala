package pl.mrz.typed

import java.net.URI
import java.util.concurrent.CountDownLatch

import akka.actor.typed.scaladsl.{ActorContext, Behaviors, TimerScheduler}
import akka.actor.typed.{ActorRef, Behavior}
import akka.persistence.typed.scaladsl.PersistentBehavior.EventHandler
import akka.persistence.typed.scaladsl.{Effect, PersistentBehavior, ReplyEffect}
import akka.persistence.typed.{ExpectingReply, PersistenceId}
import pl.mrz.typed.CheckoutManager.{CheckoutCommand, CheckoutEvent}

import scala.concurrent.duration._
import scala.util.Random

object CartManager {
  type CartStateCommandToEffect = (CartState, CartCommand[CartEvent]) ⇒ ReplyEffect[CartEvent, CartState]

  def empty(timer: TimerScheduler[CartManager.CartCommand[CartManager.CartEvent]], context: ActorContext[CartCommand[CartEvent]]): CartStateCommandToEffect =
    (_, command) => command match {
      case AddItem(item, ref) ⇒
        val event = ItemAdded(item)
        Effect.persist(event).thenReply(command)(_ => event)
      case _ => Effect.noReply
    }

  def nonEmpty(cartId: String, context: ActorContext[CartCommand[CartEvent]]): CartStateCommandToEffect =
    (_, command) => command match {
      case AddItem(item, _) ⇒
        val event = ItemAdded(item)
        Effect.persist(event).thenReply(command)(_ => event)
      case RemoveItem(id, count, _) =>
        val event = ItemRemoved(id, count)
        Effect.persist(event).thenReply(command)(_ => event)
      case CartTimeout(_) =>
        Effect.persist(TimerExpired()).thenNoReply()
      case Buy(_) =>
        val cartId = Random.nextInt.toString
        val event = CheckoutStarted(context.spawn(CheckoutManager.behavior(cartId, context.self), s"CheckoutManager|$cartId"))
        Effect.persist(event).thenReply(command)(_ => event)
      case _ => Effect.noReply
    }

  val inCheckout: CartStateCommandToEffect =
    (_, command) => command match {
      case CancelCheckout(ref) =>
        Effect.persist(CheckoutCancelled()).thenNoReply()
      case CloseCheckout(ref) =>
        Effect.persist(CheckoutClosed()).thenNoReply()
      case _ => Effect.noReply
    }

  def commandHandler(cartId: String, timer: TimerScheduler[CartManager.CartCommand[CartManager.CartEvent]], context: ActorContext[CartCommand[CartEvent]]): CartStateCommandToEffect =
    (cartState, command) => {
      cartState match {
        case inEmpty: InEmpty => empty(timer, context)(inEmpty, command)
        case inNonEmpty: InNonEmpty => nonEmpty(cartId, context)(inNonEmpty, command)
        case inCheckoutState: InCheckout => inCheckout(inCheckoutState, command)
      }
    }

  def eventHandler(timer: TimerScheduler[CartManager.CartCommand[CartManager.CartEvent]], context: ActorContext[CartCommand[CartEvent]]): EventHandler[CartState, CartEvent] = {
    case (cartState, event) ⇒
      (cartState, event) match {
        case (InEmpty(), ItemAdded(item)) =>
          timer.startSingleTimer(CartTimer, CartTimeout(context.spawnAnonymous(Behaviors.empty)), 1 second)
          InNonEmpty(Cart(Map(item.id -> item)))
        case (InNonEmpty(cart), ItemAdded(item)) => InNonEmpty(cart.addItem(item))
        case (InNonEmpty(cart), ItemRemoved(id, count)) =>
          val newCart = cart.removeItem(id, count)
          if (newCart.isEmpty) {
            timer.cancel(CartTimer)
            InEmpty()
          }
          else InNonEmpty(newCart)

        case (InNonEmpty(_), TimerExpired()) => InEmpty()
        case (InNonEmpty(cart), CheckoutStarted(_)) => InCheckout(cart)
        case (InCheckout(_), CheckoutCancelled()) => InEmpty()
        case (InCheckout(_), CheckoutClosed()) => InEmpty()
        case _ => cartState
      }
  }

  def behavior(cartId: String, onRecoveryCompleteUnlockedLatch: Option[CountDownLatch] = None): Behavior[CartCommand[CartEvent]] =
    Behaviors.withTimers(timer =>
      Behaviors.setup((context: ActorContext[CartCommand[CartEvent]]) =>
        PersistentBehavior.withEnforcedReplies(
          persistenceId = PersistenceId(s"CartManager|$cartId"),
          emptyState = InEmpty(),
          commandHandler = commandHandler(cartId, timer, context),
          eventHandler = eventHandler(timer, context)
        ).onRecoveryCompleted(_ => onRecoveryCompleteUnlockedLatch.foreach(_.countDown()))
      )
    )


  sealed trait CartCommand[Event] extends ExpectingReply[Event]

  sealed trait CartEvent

  case object CartEmpty

  final case class AddItem(item: Item, override val replyTo: ActorRef[CartEvent]) extends CartCommand[CartEvent]
  final case class RemoveItem(item: URI, count: Int, override val replyTo: ActorRef[CartEvent]) extends CartCommand[CartEvent]
  case class ItemAdded(item: Item) extends CartEvent
  case class ItemRemoved(item: URI, count: Int) extends CartEvent

  final case object CartTimer
  final case class CartTimeout(override val replyTo: ActorRef[CartEvent]) extends CartCommand[CartEvent]
  final case class TimerExpired() extends CartEvent
  final case class TimerStarted(key: CartManager.CartTimer.type) extends CartEvent

  case class Buy(override val replyTo: ActorRef[CartEvent]) extends CartCommand[CartEvent]
  case class CancelCheckout(override val replyTo: ActorRef[CartEvent]) extends CartCommand[CartEvent]
  case class CloseCheckout(override val replyTo: ActorRef[CartEvent]) extends CartCommand[CartEvent]
  case class CheckoutCancelled() extends CartEvent
  case class CheckoutStarted(checkout: ActorRef[CheckoutCommand[CheckoutEvent]]) extends CartEvent
  case class CheckoutClosed() extends CartEvent

  sealed trait CartState

  case class InEmpty() extends CartState
  case class InNonEmpty(cart: Cart) extends CartState
  case class InCheckout(cart: Cart) extends CartState
}