package pl.mrz.typed

import java.net.{ConnectException, NoRouteToHostException, PortUnreachableException, SocketException}

import akka.actor.SupervisorStrategy.{Escalate, Stop}
import akka.actor.typed.scaladsl.adapter._
import akka.actor.{ActorRef, FSM, OneForOneStrategy, Props}
import pl.mrz.http.PaymentType.PaymentType
import pl.mrz.typed.CheckoutManager.{CancelCheckout, PaymentReceivedCommand}
import pl.mrz.typed.PaymentManager._

class PaymentManager(checkout: ActorRef, serverAddress: String = "http://localhost:8080") extends FSM[State, Data] {

  startWith(WaitingForPayment, Empty)

  when(WaitingForPayment) {
    case Event(event @ DoPayment(method), _) =>
      context.actorOf(Props(new PaymentWorker(self, serverAddress))) ! event
      goto(WaitingForPaymentConfirmation) using OrderManagerData(sender)
  }

  when(WaitingForPaymentConfirmation) {
    case Event(PaymentConfirmed, OrderManagerData(orderManager)) =>
      orderManager ! PaymentConfirmed
      checkout ! PaymentReceivedCommand(context.self)
      stop
    case Event(PaymentFailed, OrderManagerData(orderManager)) =>
      orderManager ! PaymentFailed
      checkout ! CancelCheckout(self)
      stop
  }

  initialize()

  override val supervisorStrategy: OneForOneStrategy =
    OneForOneStrategy(loggingEnabled = false) {
      case e @ (_:ConnectException | _:PortUnreachableException | _:NoRouteToHostException) ⇒
        log.info(s"Connection error: ${e.getMessage}")
        notifyPaymentFailure(e)
        Stop
      case e: SocketException ⇒
        log.error(s"Socket Exception, stopping actor: ${e.getMessage}")
        notifyPaymentFailure(e)
        Stop
      case _ ⇒ Escalate
    }

  def notifyPaymentFailure(exception: Throwable): Unit = {
    stateData.asInstanceOf[OrderManagerData].orderManager ! PaymentFailed
    checkout ! CancelCheckout(self)
  }
}

object PaymentManager {
  sealed trait State
  sealed trait Data
  sealed trait Command
  sealed trait Event

  case object WaitingForPayment extends State
  case object WaitingForPaymentConfirmation extends State

  case class DoPayment(method: PaymentType) extends Command

  case object PaymentConfirmed extends Command
  case object PaymentFailed extends Command

  case object Empty extends Data
  case class OrderManagerData(orderManager: ActorRef) extends Data

}
