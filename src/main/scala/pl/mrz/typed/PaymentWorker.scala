package pl.mrz.typed

import akka.actor.{Actor, ActorRef}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, HttpResponse, StatusCodes}
import pl.mrz.typed.PaymentManager.{DoPayment, PaymentConfirmed, PaymentFailed}

class PaymentWorker(paymentManager: ActorRef, serverAddress: String) extends Actor {
  import akka.pattern.pipe
  import context.dispatcher

  override def receive: Receive = {
    case DoPayment(method) =>
      Http()(context.system).singleRequest(HttpRequest(uri = s"$serverAddress/pay/$method", method = HttpMethods.POST))
        .pipeTo(self)
      context become waitForReply
  }

  def waitForReply: Receive = {
    case HttpResponse(StatusCodes.OK, _, _, _) =>
      paymentManager ! PaymentConfirmed
      context stop self
    case HttpResponse(_, _, _, _) =>
      paymentManager ! PaymentFailed
      context stop self
  }
}