package pl.mrz.typed

import java.net.URI

case class Cart(items: Map[URI, Item]) {
  def checkoutClosed(): Cart = {
    copy(items = items.empty)
  }

  def checkoutCancelled(): Cart = {
    this
  }

  def addItem(item: Item): Cart =
    copy(items = items + (item.id ->
      items.get(item.id)
      .map(oldItem => item.copy(count = oldItem.count + item.count))
      .getOrElse(item))
    )

  def removeItem(id: URI, count: Int): Cart =
    items.get(id).map {
      case item: Item if item.count < count =>
        items + (item.id -> item.copy(count = count - item.count))
      case item: Item =>
        items - item.id
    }
      .map(updatedItems => copy(items = updatedItems ))
      .getOrElse(this)

  def isEmpty: Boolean = items.isEmpty
}

object Cart {
  val empty = Cart(items = Map.empty)
}