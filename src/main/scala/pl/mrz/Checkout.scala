package pl.mrz

import akka.actor.{Actor, ActorRef, Timers}
import pl.mrz.Cart.{CheckoutStarted => _, _}
import pl.mrz.Checkout._
import pl.mrz.LoggingHelper.log

import scala.concurrent.duration._

class Checkout extends Actor with Timers {
  var cart: ActorRef = _

  override def receive: Receive = {
    case StartCheckout =>
      log(CheckoutStarted)
      cart = sender()
      timers.startSingleTimer(CheckoutTimer, CancelCheckout, 1 second)
      context become selectingDelivery
  }

  def selectingDelivery: Receive = {
    case SelectDelivery(deliveryMethodId) =>
      log(DeliverySelected, deliveryMethodId)
      context become selectingPaymentMethod
    case CancelCheckout =>
      context become cancelled
      self ! CancelCheckout
  }

  def selectingPaymentMethod: Receive = {
    case SelectPayment(paymentId) =>
      log(PaymentSelected, paymentId)
      timers.cancel(CheckoutTimer)
      timers.startSingleTimer(PaymentTimer, CancelCheckout, 1 second)
      context become processingPayment
    case CancelCheckout =>
      context become cancelled
      self ! CancelCheckout
  }

  def processingPayment: Receive = {
    case CancelCheckout =>
      context become cancelled
      self ! CancelCheckout
    case PaymentReceived =>
      log(PaymentReceived)
      context become closed
      self ! CloseCheckout
  }

  def cancelled: Receive = {
    case CancelCheckout =>
      cart ! CancelCheckout
      context stop self
  }

  def closed: Receive = {
    case CloseCheckout =>
      cart ! CloseCheckout
      context stop self
  }
}

case object Checkout {
  sealed trait Command
  sealed trait Event

  case class SelectDelivery(deliveryMethodId: String) extends Command
  case class SelectPayment(paymentId: String) extends Command

  case object CheckoutStarted extends Event
  case class DeliverySelected(deliveryMethod: String) extends Event
  case class PaymentSelected(payment: String) extends Event
  case object DeliveryAndPaymentMethodSelected extends Event
  case object PaymentReceived extends Event

  case object CheckoutTimer
  case object PaymentTimer
}
