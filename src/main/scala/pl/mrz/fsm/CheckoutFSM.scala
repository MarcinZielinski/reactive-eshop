package pl.mrz.fsm

import akka.actor.{ActorRef, FSM, Props}
import pl.mrz.fsm.CartFSM._
import pl.mrz.fsm.CheckoutFSM._

import scala.concurrent.duration._

class CheckoutFSM(cart: ActorRef, checkoutExpirationTime: FiniteDuration = 100 millis, paymentExpirationTime: FiniteDuration = 100 millis)
  extends FSM[CheckoutState, CheckoutData] {

  startWith(SelectingDelivery, CheckoutParameters())

  when(SelectingDelivery, checkoutExpirationTime) {
    case Event(CheckoutCanceled | StateTimeout, _) =>
      goto(Canceled)
    case Event(SelectDelivery(method), checkoutData: CheckoutParameters) =>
      goto(SelectingPayment) using checkoutData.copy(delivery = method)
  }

  when(SelectingPayment, checkoutExpirationTime) {
    case Event(CheckoutCanceled | StateTimeout, _) =>
      goto(Canceled)
    case Event(SelectPayment(method), checkoutData: CheckoutParameters) =>
      sender ! PaymentServiceStarted(context.actorOf(Props(new PaymentFSM(self))))
      goto(ProcessingPayment) using checkoutData.copy(payment = method)
  }

  when(ProcessingPayment, paymentExpirationTime) {
    case Event(CheckoutCanceled | StateTimeout, _) =>
      goto(Canceled)
    case Event(PaymentReceived, _) =>
      cart ! CheckoutClosed
      goto(Closed)
  }

  when(Canceled) {
    case _ => stop()
  }

  when(Closed) {
    case _ => stop()
  }

  initialize()
}

object CheckoutFSM {
  sealed trait CheckoutData
  sealed trait CheckoutState

  case class CheckoutParameters(delivery: String = "", payment: String = "") extends CheckoutData

  sealed trait Event
  case class PaymentServiceStarted(payment: ActorRef) extends Event
  case object PaymentReceived extends Event
  case class DeliverySelected(deliveryMethod: String) extends Event


  sealed trait Command
  case class SelectDelivery(deliveryMethodId: String) extends Command
  case class SelectPayment(paymentId: String) extends Command

  case object SelectingDelivery extends CheckoutState
  case object SelectingPayment extends CheckoutState
  case object ProcessingPayment extends CheckoutState
  case object Canceled extends CheckoutState
  case object Closed extends CheckoutState

}