package pl.mrz.fsm

import akka.actor.{ActorRef, FSM, Props}
import pl.mrz.fsm.CartFSM._
import pl.mrz.fsm.OrderManagerFSM.{AddItem, Buy, RemoveItem}

import scala.concurrent.duration._

class CartFSM(orderManager: ActorRef, cartExpirationTime: FiniteDuration = 100 millis) extends FSM[CartState, CartData] {

  startWith(Empty, CartContent(List.empty))

  when(Empty) {
    case Event(AddItem(itemId), CartContent(content)) =>
      sender ! ItemAdded(itemId)
      goto(NonEmpty) using CartContent(content :+ itemId)
  }

  when(NonEmpty, cartExpirationTime) {
    case Event(AddItem(itemId), CartContent(content)) =>
      sender ! ItemAdded(itemId)
      stay using CartContent(content :+ itemId)
    case Event(StateTimeout, _) =>
      goto(Empty) using CartContent(List.empty)
    case Event(RemoveItem(itemId), CartContent(list)) if list.length == 1 && list.contains(itemId) =>
      sender ! ItemRemoved(itemId)
      goto(Empty) using CartContent(List.empty)
    case Event(RemoveItem(itemId), CartContent(content)) =>
      sender ! ItemRemoved(itemId)
      stay using CartContent(content diff List(itemId))
    case Event(Buy, content: CartContent) =>
      sender ! CheckoutStarted(context.actorOf(Props(new CheckoutFSM(self))))
      goto(InCheckout) using content
  }

  when(InCheckout) {
    case Event(CheckoutCanceled, content: CartContent) =>
      goto(NonEmpty) using content
    case Event(CheckoutClosed, _) =>
      orderManager ! CartEmpty
      goto(Empty) using CartContent(List.empty)
  }

  initialize()
}

object CartFSM {
  sealed trait CartState
  sealed trait CartData

  case class CartContent(items: List[String]) extends CartData

  sealed trait Event
  case class ItemAdded(itemId: String) extends Event
  case class ItemRemoved(itemId: String) extends Event
  case class CheckoutStarted(cart: ActorRef) extends Event
  case object CartEmpty extends Event
  case object CheckoutStarted extends Event
  case object CheckoutCanceled extends Event
  case object CheckoutClosed extends Event

  case object Empty extends CartState
  case object NonEmpty extends CartState
  case object InCheckout extends CartState

}