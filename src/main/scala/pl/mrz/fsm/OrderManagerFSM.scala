package pl.mrz.fsm

import akka.actor.{ActorRef, FSM, Props}
import pl.mrz.fsm.CartFSM.{CheckoutStarted, ItemAdded}
import pl.mrz.fsm.CheckoutFSM.{PaymentServiceStarted, SelectDelivery, SelectPayment}
import pl.mrz.fsm.OrderManagerFSM._
import pl.mrz.fsm.PaymentFSM.{DoPayment, PaymentConfirmed}

class OrderManagerFSM(id: String) extends FSM[State, Data] {
  startWith(Uninitialized, Empty())

  when(Uninitialized) {
    case x @ Event(AddItem(_) | RemoveItem(_), Empty()) =>
      val cart = context.actorOf(Props(new CartFSM(self)))
      cart ! x.event
      stay using CartDataWithSender(cart, sender)
    case Event(ItemAdded(_), CartDataWithSender(cart, client)) =>
      client ! Done
      goto(Open) using CartData(cart)
  }

  when(Open) {
    case x @ Event(AddItem(_) | RemoveItem(_), CartData(cart)) =>
      cart ! x.event
      stay using CartDataWithSender(cart, sender)
    case Event(ItemAdded(_), CartDataWithSender(cart, client)) =>
      client ! Done
      stay using CartData(cart)
    case Event(Buy, CartData(cart)) =>
      cart ! Buy
      stay using CartDataWithSender(cart, sender)
    case Event(CheckoutStarted(checkout), CartDataWithSender(_, client)) =>
      client ! Done
      goto(InCheckout) using InCheckoutData(checkout)
  }

  when(InCheckout) {
    case Event(SelectDeliveryAndPaymentMethod(delivery, payment), InCheckoutData(checkout)) =>
      checkout ! SelectDelivery(delivery)
      checkout ! SelectPayment(payment)
      stay using InCheckoutDataWithSender(checkout, sender)
    case Event(PaymentServiceStarted(payment), InCheckoutDataWithSender(_, client)) =>
      client ! Done
      goto(InPayment) using InPaymentData(payment)
  }

  when(InPayment) {
    case Event(Pay, InPaymentData(payment)) =>
      payment ! DoPayment
      stay using InPaymentDataWithSender(payment, sender)
    case Event(PaymentConfirmed, InPaymentDataWithSender(_, client)) =>
      client ! Done
      goto(Finished) using Empty()
  }

  when(Finished) {
    case _ => stay
  }

  initialize()
}

object OrderManagerFSM {
  sealed trait State
  case object Uninitialized extends State
  case object Open          extends State
  case object InCheckout    extends State
  case object InPayment     extends State
  case object Finished      extends State

  sealed trait Command
  case class AddItem(id: String)                                               extends Command
  case class RemoveItem(id: String)                                            extends Command
  case class SelectDeliveryAndPaymentMethod(delivery: String, payment: String) extends Command
  case object Buy                                                              extends Command
  case object Pay                                                              extends Command

  sealed trait Ack
  case object Done extends Ack //trivial ACK

  sealed trait Data
  case class Empty()                                                           extends Data
  case class CartData(cartRef: ActorRef)                                       extends Data
  case class CartDataWithSender(cartRef: ActorRef, sender: ActorRef)           extends Data
  case class InCheckoutData(checkoutRef: ActorRef)                             extends Data
  case class InCheckoutDataWithSender(checkoutRef: ActorRef, sender: ActorRef) extends Data
  case class InPaymentData(paymentRef: ActorRef)                               extends Data
  case class InPaymentDataWithSender(paymentRef: ActorRef, sender: ActorRef)   extends Data
}
