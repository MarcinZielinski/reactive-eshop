package pl.mrz.fsm

import akka.actor.{ActorRef, FSM}
import pl.mrz.fsm.CheckoutFSM.PaymentReceived
import pl.mrz.fsm.PaymentFSM._

class PaymentFSM(checkout: ActorRef) extends FSM[State, Data] {
  startWith(WaitingForPayment, Empty)

  when(WaitingForPayment) {
    case Event(DoPayment, Empty) =>
      sender ! PaymentConfirmed
      checkout ! PaymentReceived
      stop
  }
}

object PaymentFSM {
  sealed trait State
  sealed trait Data
  sealed trait Command
  sealed trait Event

  case object WaitingForPayment extends State

  case object DoPayment extends Command
  case object PaymentConfirmed extends Command

  case object Empty extends Data
}