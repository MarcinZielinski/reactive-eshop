package pl.mrz

import akka.actor.{Actor, ActorRef, Props, Timers}
import akka.event.LoggingReceive
import pl.mrz.Cart._
import pl.mrz.LoggingHelper.log

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration._

class Cart extends Actor with Timers {
  private val itemList = ArrayBuffer[String]()

  override def receive: Receive = empty

  def empty: Receive = LoggingReceive {
    case AddItem(itemId) =>
      itemList += itemId
      log(ItemAdded, itemId)
      timers.startSingleTimer(CartTimer, ResetCart, 1 second)
      context become nonEmpty
  }

  def nonEmpty: Receive = LoggingReceive {
    case AddItem(itemId) =>
      itemList += itemId
      log(ItemAdded, itemId)
    case RemoveItem(itemId) =>
      itemList -= itemId
      log(ItemRemoved, itemId)
      if (itemList.isEmpty)
        context become receive
    case StartCheckout if itemList.nonEmpty =>
      timers.cancel(CartTimer)
      val checkout = context.actorOf(Props[Checkout])
      sender() ! checkout
      checkout ! StartCheckout
      context become inCheckout
    case ResetCart => // cart timer expired
      itemList.clear()
      log(CartTimerExpired)
      context become empty
  }

  def inCheckout: Receive = LoggingReceive {
    case CancelCheckout =>
      log(CheckoutCanceled)
      context become nonEmpty
    case CloseCheckout =>
      itemList.clear()
      log(CheckoutClosed)
      context become empty
  }
}

case object Cart {
  sealed trait Event
  sealed trait Command

  case class AddItem(itemId: String) extends Command
  case class RemoveItem(itemId: String) extends Command
  case class StartCheckout(checkout: ActorRef) extends Command
  case object CancelCheckout extends Command
  case object CloseCheckout extends Command
  case object ResetCart extends Command

  case class ItemAdded(itemId: String) extends Event
  case class ItemRemoved(itemId: String) extends Event
  case object CheckoutStarted extends Event
  case object CheckoutCanceled extends Event
  case object CheckoutClosed extends Event
  case object CartTimerExpired extends Event

  case object CartTimer
}
