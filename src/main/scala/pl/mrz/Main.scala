package pl.mrz

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import pl.mrz.Cart.{AddItem, RemoveItem, StartCheckout}
import pl.mrz.Checkout.{PaymentReceived, SelectDelivery, SelectPayment}
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.{Await, ExecutionContextExecutor}
import scala.concurrent.duration.Duration


object Main extends App {
  val actorSystem = ActorSystem("eshop")
  implicit val timeout: Timeout = Timeout(10, TimeUnit.SECONDS)
  implicit val executionContext: ExecutionContextExecutor = actorSystem.dispatcher

  val cart = actorSystem actorOf Props[Cart]

  cart ! AddItem("1")
  cart ! AddItem("2")
  Thread.sleep(1100) // trigger CartTimer functionality
  cart ! RemoveItem("1")
  cart ! AddItem("2")
  cart ! AddItem("3")
  cart ! RemoveItem("2")
  cart ! AddItem("12")
  val checkout: ActorRef = Await.result(cart ? StartCheckout, Duration(1, TimeUnit.SECONDS)).asInstanceOf[ActorRef]
  checkout ! StartCheckout
  checkout ! SelectDelivery("letter")
  checkout ! SelectPayment("bank transfer")
  Thread.sleep(1100) // trigger PaymentTimer
  val checkout2 = Await.result(cart ? StartCheckout, Duration(1, TimeUnit.SECONDS)).asInstanceOf[ActorRef]
  checkout2 ! StartCheckout
  checkout2 ! SelectDelivery("pigeon")
  checkout2 ! SelectPayment("cash")
  checkout2 ! PaymentReceived

  Thread.sleep(700000)

  Await.result(actorSystem.terminate(), Duration.Inf)
}