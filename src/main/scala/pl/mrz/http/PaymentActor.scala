package pl.mrz.http

import akka.actor.Actor
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import pl.mrz.http.PaymentType.{PayPal, PayU, VISA}

class PaymentActor extends Actor {
  override def receive: Receive = {
    case VISA => sender ! HttpResponse(StatusCodes.OK)
    case PayPal => sender ! HttpResponse(StatusCodes.OK)
    case PayU => sender ! HttpResponse(StatusCodes.OK)
  }
}