package pl.mrz.http

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer

import scala.concurrent.ExecutionContextExecutor
import scala.io.StdIn
import pl.mrz.http.PaymentType._
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.duration._

object PaymentServer {
  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  implicit val timeout: Timeout = 1 second

  val route: Route =
    pathPrefix("pay") {
      post {
        val paymentActor = system.actorOf(Props[PaymentActor])
        path(VISA.method) {
          complete {
            (paymentActor ? VISA).mapTo[HttpResponse]
          }
        } ~
          path(PayPal.method) {
            complete {
              (paymentActor ? PayPal).mapTo[HttpResponse]
            }
          } ~
          path(PayU.method) {
            complete {
              (paymentActor ? PayU).mapTo[HttpResponse]
            }
          }
      }
    }

  def main(args: Array[String]) {
    // `route` will be implicitly converted to `Flow` using `RouteResult.route2HandlerFlow`
    val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)
    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }
}
