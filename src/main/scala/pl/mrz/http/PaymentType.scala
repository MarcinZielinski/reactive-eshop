package pl.mrz.http


case object PaymentType {
  sealed trait PaymentType {
    val method: String

    override def toString: String = method
  }

  case object VISA extends PaymentType {
    override val method = "VISA"
  }
  case object PayPal extends PaymentType {
    override val method: String = "PAYPAL"
  }
  case object PayU extends PaymentType {
    override val method: String = "PAYU"
  }
}