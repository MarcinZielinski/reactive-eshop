package pl.mrz.http

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{Matchers, WordSpec}

class PaymentServerTest extends WordSpec with Matchers with ScalatestRouteTest {
  "PaymentServer" should {
    "return OK response for VISA payment" in {
      Post(s"/pay/${PaymentType.VISA.method}") ~> PaymentServer.route ~> check {
        status shouldEqual StatusCodes.OK
      }
    }

    "not handle for non existing payment" in {
      Post(s"/pay/trade") ~> PaymentServer.route ~> check {
        handled shouldBe false
      }
    }
  }
}
