package pl.mrz.http

import java.net.URI
import java.util.UUID

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.testkit.{TestFSMRef, TestKit}
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import org.scalatest._
import pl.mrz.typed.OrderManager.{AddItem, Data, Done, State}
import pl.mrz.typed.{Item, OrderManager}
import pl.mrz.http.OrderManagerTest.journalId

import scala.concurrent.Await
import scala.concurrent.duration._

class OrderManagerTest extends TestKit(ActorSystem("ActorSystem",
  ConfigFactory.load(
    ConfigFactory.parseString(s"akka.persistence.journal.leveldb.dir=target/journal$journalId")
      .withFallback(ConfigFactory.load())))) with FlatSpecLike with Matchers {
  implicit val timeout: Timeout = 3.second

  "OrderManager" should "retrieve item from remote product catalog" in {
    val config = ConfigFactory.load()
    val productSystem = ActorSystem("ProductCatalog", config.getConfig("productcatalog").withFallback(config))
    productSystem.actorOf(ProductCatalog.props(new SearchService()), "productcatalog")


    val orderManager = TestFSMRef[State, Data, OrderManager](new OrderManager("OrderManager"))
    val response = Await.result(orderManager ? AddItem(Item(URI.create("1"), "cream", "gerber", 2, 1)), 5 seconds)
    productSystem.terminate()
    Await.ready(productSystem.whenTerminated, 5 seconds)
    assert(response == Done)
  }
}

object OrderManagerTest {
  val journalId: UUID = UUID.randomUUID()
}
