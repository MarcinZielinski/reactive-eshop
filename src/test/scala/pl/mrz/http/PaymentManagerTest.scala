package pl.mrz.http

import java.net.ConnectException

import akka.actor.{Actor, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import akka.testkit.{TestFSMRef, TestProbe}
import akka.util.Timeout
import org.scalatest.{GivenWhenThen, Matchers, WordSpecLike}
import pl.mrz.fsm.TestFsmBase
import pl.mrz.http.PaymentType._
import pl.mrz.typed.CheckoutManager.PaymentReceivedCommand
import pl.mrz.typed.PaymentManager
import pl.mrz.typed.PaymentManager.DoPayment

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.reflect.ClassTag

class PaymentManagerTest extends TestFsmBase with WordSpecLike with Matchers with GivenWhenThen {
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher
  implicit val timeout: Timeout = 1 second

  val route: Route =
    pathPrefix("pay") {
      post {
        path(VISA.method) {
          complete {
            HttpResponse(StatusCodes.OK)
          }
        }
      }
    }


  private val serverHost = "localhost"
  private val serverPort = 8080
  private val serverAddress = s"http://$serverHost:$serverPort"

  "PaymentManager" should {
    "pass integration test" in {
      val bindingFuture: Future[Http.ServerBinding] = Http().bindAndHandle(route, serverHost, serverPort)

      Given("actors")
      val checkout = TestProbe()
      val paymentManager = TestFSMRef(new PaymentManager(checkout.ref, serverAddress))

      When("Payment is being made by client")
      paymentManager ! DoPayment(PaymentType.VISA)

      Then("Payment manager should send message to server and respond to checkout")
      checkout.expectMsgType(ClassTag[PaymentReceivedCommand](classOf[PaymentReceivedCommand]))

      bindingFuture
        .flatMap(_.unbind()) // trigger unbinding from the port
    }

    "supervise correctly" in {
      Given("actors")
      val checkout = TestProbe()
      val orderManager = TestProbe()
      val paymentManager = TestFSMRef(new PaymentManager(checkout.ref, serverAddress))
      paymentManager.setState(stateData = PaymentManager.OrderManagerData(orderManager.ref))
      val paymentWorker = paymentManager.underlyingActor.context.actorOf(Props(new Actor {
        def receive: PartialFunction[Any, Unit] = {
          case "error" => throw new ConnectException(":(")
        }
      }))

      When("ConnectionException is thrown by worker")
      watch(paymentWorker) // let's observe worker
      paymentWorker ! "error"

      Then("Worker should be stopped")
      expectTerminated(paymentWorker)
    }
  }
}
