package pl.mrz.http

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import org.scalatest.{AsyncFlatSpec, FlatSpec, Matchers}
import pl.mrz.http.ProductCatalog.GetItems

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration._

class ProductCatalogRemoteTest extends FlatSpec with Matchers {
  implicit val timeout: Timeout = 3.second
  "A remote Product Catalog" should "return search results" in {
    val config = ConfigFactory.load()
    val query  = GetItems("gerber", List("cream"))
    val actorSystem = ActorSystem("ProductCatalog", config.getConfig("productcatalog").withFallback(config))
    actorSystem.actorOf(ProductCatalog.props(new SearchService()), "productcatalog")
    val anotherActorSystem = ActorSystem("another")
    val productCatalog =
      anotherActorSystem.actorSelection("akka.tcp://ProductCatalog@127.0.0.1:2554/user/productcatalog")
    implicit val executionContext: ExecutionContext = actorSystem.dispatcher
    val res = for {
      productCatalogActorRef <- productCatalog.resolveOne()
      items                  <- (productCatalogActorRef ? query).mapTo[ProductCatalog.Items]
    } yield {
      items
    }
    val items = Await.result(res, 5 seconds)
    actorSystem.terminate()
    Await.ready(actorSystem.whenTerminated, 5 seconds)
    assert(items.items.size == 10)
  }
}
