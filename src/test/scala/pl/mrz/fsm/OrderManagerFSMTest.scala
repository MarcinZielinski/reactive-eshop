package pl.mrz.fsm

import scala.concurrent.duration._
import akka.actor.ActorSystem
import akka.pattern.ask
import akka.testkit.{TestFSMRef, TestKit}
import akka.util.Timeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class OrderManagerFSMTest
  extends TestFsmBase
    with WordSpecLike
    with BeforeAndAfterAll
    with ScalaFutures
    with Matchers {

  implicit val timeout: Timeout = 1.second

  "An order manager" must {
    "supervise whole order process" in {
      import pl.mrz.fsm.OrderManagerFSM._

      def sendMessageAndValidateState(
                                       orderManager: TestFSMRef[State, Data, OrderManagerFSM],
                                       message: OrderManagerFSM.Command,
                                       expectedState: OrderManagerFSM.State
                                     ): Unit = {
        (orderManager ? message).mapTo[OrderManagerFSM.Ack].futureValue shouldBe Done
        orderManager.stateName shouldBe expectedState
      }

      val orderManager = TestFSMRef[State, Data, OrderManagerFSM](new OrderManagerFSM("orderManagerId"))
      orderManager.stateName shouldBe Uninitialized

      sendMessageAndValidateState(orderManager, AddItem("rollerblades"), Open)

      sendMessageAndValidateState(orderManager, Buy, InCheckout)

      sendMessageAndValidateState(orderManager, SelectDeliveryAndPaymentMethod("paypal", "inpost"), InPayment)

      sendMessageAndValidateState(orderManager, Pay, Finished)
    }
  }

}