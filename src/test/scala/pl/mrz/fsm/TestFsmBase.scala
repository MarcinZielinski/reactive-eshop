package pl.mrz.fsm

import akka.actor.ActorSystem
import akka.testkit.TestKit
import com.typesafe.config.ConfigFactory

class TestFsmBase extends TestKit(ActorSystem("test-system", ConfigFactory.load("application-fsm-test.conf")))
