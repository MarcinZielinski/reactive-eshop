package pl.mrz.fsm

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{TestFSMRef, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, Matchers, WordSpecLike}
import pl.mrz.fsm.CheckoutFSM.PaymentReceived
import pl.mrz.fsm.PaymentFSM._

class PaymentFSMTest extends TestFsmBase with WordSpecLike with Matchers with BeforeAndAfter with BeforeAndAfterAll {

  var payment: TestFSMRef[State, Data, PaymentFSM] = _
  var checkout: TestProbe = _

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  before {
    checkout = TestProbe()
    payment = TestFSMRef(new PaymentFSM(checkout.ref))
  }

  "Payment" should {
    "start in WaitingForPayment" in {
      payment.stateData shouldBe Empty
      payment.stateName shouldBe WaitingForPayment
    }

    "send to parent checkout PaymentReceived" in {
      payment ! DoPayment
      checkout expectMsg PaymentReceived
    }

    "send back to sender ack PaymentConfirmed" in {
      val testSender = TestProbe()
      payment.!(DoPayment)(testSender.ref)

      testSender expectMsg PaymentConfirmed
    }
  }
}
