package pl.mrz.fsm

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestFSMRef, TestKit, TestProbe}
import com.typesafe.config.ConfigFactory
import org.scalatest.concurrent.Eventually
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, Matchers, WordSpecLike}
import pl.mrz.fsm.CartFSM._
import pl.mrz.fsm.CheckoutFSM._

import scala.concurrent.duration._

class CheckoutFSMTest extends TestFsmBase with WordSpecLike with Matchers with BeforeAndAfterAll with BeforeAndAfter with Eventually {

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  var checkout: TestFSMRef[CheckoutState, CheckoutData, CheckoutFSM] = _
  var cart: TestProbe = _

  before {
    cart = TestProbe()
    checkout = TestFSMRef(new CheckoutFSM(cart.ref,20 millis, 20 millis))
  }

  "Checkout" should {
    "checkout should send CheckoutClosed msg to parent cart" in {
      checkout ! SelectDelivery("delivery")
      checkout ! SelectPayment("payment")
      checkout ! PaymentReceived

      cart expectMsg CheckoutClosed
    }

    "start in SelectingDelivery" in {
      checkout.stateName shouldBe SelectingDelivery
      checkout.stateData shouldBe CheckoutParameters()
    }

    "be cancelable in SelectingDelivery" in {
      checkout ! CheckoutCanceled

      checkout.stateName shouldBe Canceled
    }

    "goto Canceled in SelectingDelivery after timer expires" in {
      checkout.stateName shouldBe SelectingDelivery
      eventually {
        checkout.stateName shouldBe Canceled
      }
    }

    "go to SelectingPayment after delivery selection" in {
      checkout ! SelectDelivery("delivery")

      checkout.stateName shouldBe SelectingPayment
      checkout.stateData shouldBe CheckoutParameters("delivery")
    }

    "be cancelable after delivery selection" in {
      checkout ! SelectDelivery("delivery")
      checkout ! CheckoutCanceled

      checkout.stateName shouldBe Canceled
    }

    "goto Canceled after delivery selection and timer expires" in {
      checkout ! SelectDelivery("delivery")

      checkout.stateName shouldBe SelectingPayment
      eventually {
        checkout.stateName shouldBe Canceled
      }
    }

    "go to ProcessingPayment after payment selection" in {
      checkout ! SelectDelivery("delivery")
      checkout ! SelectPayment("payment")

      checkout.stateName shouldBe ProcessingPayment
      checkout.stateData shouldBe CheckoutParameters("delivery", "payment")
    }

    "be closed after receiving payment" in {
      checkout ! SelectDelivery("delivery")
      checkout ! SelectPayment("payment")
      checkout ! PaymentReceived

      checkout.stateName shouldBe Closed
    }

    "be cancelable after payment selection" in {
      checkout ! SelectDelivery("delivery")
      checkout ! SelectPayment("payment")
      checkout ! CheckoutCanceled

      checkout.stateName shouldBe Canceled
    }

    "goto Canceled after payment selection and timer expires" in {
      checkout ! SelectDelivery("delivery")
      checkout ! SelectPayment("payment")

      checkout.stateName shouldBe ProcessingPayment
      eventually {
        checkout.stateName shouldBe Canceled
      }
    }
  }
}
