package pl.mrz.fsm

import akka.actor.ActorSystem
import akka.testkit.{TestFSMRef, TestKit, TestProbe}
import com.typesafe.config.ConfigFactory
import org.scalatest._
import org.scalatest.concurrent.Eventually
import pl.mrz.fsm.CartFSM._
import pl.mrz.fsm.OrderManagerFSM.{AddItem, Buy, RemoveItem}

import scala.concurrent.duration._

class CartFSMTest extends TestFsmBase with WordSpecLike with Matchers with BeforeAndAfter with BeforeAndAfterAll with Eventually {

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  var cart: TestFSMRef[CartState, CartData, CartFSM] = _

  before {
    val orderManager = TestProbe()
    cart = TestFSMRef(new CartFSM(orderManager.ref, 20 millis))
  }

  "Cart" should {
    "start in Empty" in {
      cart.stateName shouldBe Empty
      cart.stateData shouldBe CartContent(List.empty)
    }

    "return to Empty after adding and removing one item" in {
      cart ! AddItem("1")
      cart ! RemoveItem("1")

      cart.stateName shouldBe Empty
    }

    "stay in Empty and ignore checkout" in {
      cart ! Buy

      cart.stateName shouldBe Empty
    }

    "go to Empty after adding product and timer expires" in {
      cart ! AddItem("1")

      eventually {
        cart.stateName shouldBe Empty
        cart.stateData shouldBe CartContent(List.empty)
      }
    }

    "go to Empty after checkout and checkout closed" in {
      cart ! AddItem("1")
      cart ! Buy
      cart ! CheckoutClosed

      cart.stateName shouldBe Empty
      cart.stateData shouldBe CartContent(List.empty)
    }

    "go to NonEmpty after adding product" in {
      cart ! AddItem("1")

      cart.stateName shouldBe NonEmpty
      cart.stateData shouldBe CartContent(List("1"))
    }

    "stay in NonEmpty after adding item and removing wrong item" in {
      cart ! AddItem("1")
      cart ! RemoveItem("2")

      cart.stateName shouldBe NonEmpty
      cart.stateData shouldBe CartContent(List("1"))
    }

    "stay in NonEmpty after adding two same products and removing one product" in {
      cart ! AddItem("1")
      cart ! AddItem("1")
      cart ! RemoveItem("1")

      cart.stateName shouldBe NonEmpty
      cart.stateData shouldBe CartContent(List("1"))
    }

    "go to NonEmpty after checkout and checkout cancel" in {
      cart ! AddItem("1")
      cart ! AddItem("2")
      cart ! Buy
      cart ! CheckoutCanceled

      cart.stateName shouldBe NonEmpty
      cart.stateData shouldBe CartContent(List("1","2"))
    }

    "go to InCheckout after adding product and checkout" in {
      cart ! AddItem("1")
      cart ! Buy

      cart.stateName shouldBe InCheckout
    }
  }
}
