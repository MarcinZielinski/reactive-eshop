package pl.mrz.typed

import java.net.URI

import akka.actor.testkit.typed.scaladsl.TestInbox
import org.scalatest.{GivenWhenThen, Matchers, WordSpecLike}

class CartTest extends WordSpecLike with Matchers with GivenWhenThen {
  "Cart" should {
    "add item" in {
      Given("empty cart")
      val item = Item(URI.create("1"), "Product", "Brand", 1, 1)
      var cart = CartTest.empty
      When("item is added")
      cart = cart.addItem(item)
      Then("cart should have that item")
      cart.items(item.id) shouldEqual item
    }

    "remove item" in {
      Given("cart with item")
      val item = Item(URI.create("1"), "Product", "Brand", 1, 1)
      var cart = CartTest.emptyWithMap(Map(item.id -> item))
      When("item is removed")
      cart = cart.removeItem(item.id, 1)
      Then("cart should have that item")
      cart.items.get(item.id) shouldBe None
    }

    "remain in the same state" in {
      Given("empty cart")
      val item = Item(URI.create("1"), "Product", "Brand", 1, 1)
      val cart = CartTest.empty
      When("some item is removed")
      val cart2 = cart.removeItem(item.id, 1)
      Then("cart should remain the same")
      cart shouldBe cart2
    }

    "when adding existing item increase only count" in {
      Given("cart with one item")
      val item = Item(URI.create("1"), "Product", "Brand", 1, 1)
      var cart = CartTest.emptyWithMap(Map(item.id -> item))
      When("add the same item")
      cart = cart.addItem(item)
      Then("cart should have one item but with increased count")
      cart.items.size shouldBe 1
      cart.items(item.id).count shouldBe 2
    }
  }
}

object CartTest {
  val empty: Cart = Cart.empty
  def emptyWithMap(items: Map[URI, Item]): Cart = Cart(items)
}
