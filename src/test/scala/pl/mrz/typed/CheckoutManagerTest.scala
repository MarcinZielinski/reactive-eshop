package pl.mrz.typed

import java.util.UUID
import java.util.concurrent.CountDownLatch

import akka.actor.testkit.typed.scaladsl.{ActorTestKit, ManualTime, TestProbe}
import akka.actor.typed.ActorRef
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import pl.mrz.typed.CartManager.{CartCommand, CartEvent}
import pl.mrz.typed.CheckoutManager._
import pl.mrz.typed.CheckoutManagerTest._

import scala.concurrent.duration._
import scala.reflect.ClassTag

class CheckoutManagerTest extends WordSpecLike with Matchers with BeforeAndAfterAll {

  var testKit = ActorTestKit("TestActorSystem",
    ConfigFactory.load(
      ConfigFactory.parseString(s"akka.persistence.journal.leveldb.dir=target/journal$journalId")
        .withFallback(ConfigFactory.load())))

  var orderManager: TestProbe[CheckoutEvent] = testKit.createTestProbe[CheckoutEvent](s"OrderManager${UUID.randomUUID()}")
  var cartManager: TestProbe[CartCommand[CartEvent]] = testKit.createTestProbe[CartCommand[CartEvent]](s"CartManager${UUID.randomUUID()}")
  var manualTime: ManualTime = ManualTime()(testKit.system)
  var checkoutManager: ActorRef[CheckoutCommand[CheckoutEvent]] = _

  override protected def afterAll(): Unit = testKit.shutdownTestKit()

  "CheckoutManager" should {
    "pass integration test" in {
      val latch = new CountDownLatch(1)
      checkoutManager = testKit.spawn(CheckoutManager.behavior(s"CheckoutManager${UUID.randomUUID()}", cartManager.ref, onRecoveryCompleteUnlockedLatch = Some(latch)))
      latch.await()

      checkoutManager ! SelectDelivery(deliveryMethod, orderManager.ref)
      orderManager.expectNoMessage()

      simulateSystemShutdownAndRestart()

      checkoutManager ! SelectPayment(paymentMethod, orderManager.ref)
      orderManager.expectMessageType(ClassTag[PaymentServiceStarted](classOf[PaymentServiceStarted]))

      manualTime.timePasses(2 second)

      checkoutManager ! SelectPayment(paymentMethod, orderManager.ref)
      cartManager.expectNoMessage()
      checkoutManager ! PaymentReceivedCommand(orderManager.ref)
      cartManager.expectNoMessage()
    }
  }

  def simulateSystemShutdownAndRestart(): Unit = {
    Thread.sleep(100) // let events be persisted
    testKit.shutdownTestKit()
    testKit = ActorTestKit("ActorSystem",
      ConfigFactory.load(
        ConfigFactory.parseString(s"akka.persistence.journal.leveldb.dir=target/journal$journalId")
          .withFallback(ConfigFactory.load())))
    orderManager = testKit.createTestProbe[CheckoutEvent]("OrderManager")
    cartManager = testKit.createTestProbe[CartCommand[CartEvent]]("CartManager")
    manualTime = ManualTime()(testKit.system)
    val latch = new CountDownLatch(1)
    checkoutManager = testKit.spawn(CheckoutManager.behavior("CheckoutManager", cartManager.ref, onRecoveryCompleteUnlockedLatch = Some(latch)))
    latch.await()
  }
}

object CheckoutManagerTest {
  val deliveryMethod = "deliveryMethod"
  val paymentMethod = "paymentMethod"
  val journalId: UUID = UUID.randomUUID()
}
