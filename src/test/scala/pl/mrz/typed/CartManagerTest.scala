package pl.mrz.typed

import java.net.URI
import java.util.concurrent.CountDownLatch

import akka.actor.testkit.typed.scaladsl.{ActorTestKit, ActorTestKitBase, ManualTime, TestProbe}
import akka.actor.typed.ActorRef
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import pl.mrz.typed.CartManagerTest._

import scala.concurrent.duration._

class CartManagerTest extends WordSpecLike with Matchers with BeforeAndAfterAll with ScalaFutures {

  var testKit = ActorTestKit(ActorTestKitBase.testNameFromCallStack())

  override def afterAll(): Unit = testKit.shutdownTestKit()

  var orderManager: TestProbe[CartManager.CartEvent] = testKit.createTestProbe[CartManager.CartEvent]()
  var cartManager: ActorRef[CartManager.CartCommand[CartManager.CartEvent]] = testKit.spawn(CartManager.behavior("CartManager"))

  var manualTime: ManualTime = ManualTime()(testKit.system)

  "Cart manager actor" should {
    "pass integration test" in {
      cartManager ! CartManager.AddItem(sampleItem, orderManager.ref)
      orderManager.expectMessage(CartManager.ItemAdded(sampleItem))

      cartManager ! CartManager.RemoveItem(sampleItem.id, sampleItem.count, orderManager.ref)
      orderManager.expectMessage(CartManager.ItemRemoved(sampleItem.id, sampleItem.count))

      cartManager ! CartManager.RemoveItem(sampleItem.id, sampleItem.count, orderManager.ref)
      orderManager.expectNoMessage()

      cartManager ! CartManager.AddItem(sampleItem, orderManager.ref)
      orderManager.expectMessage(CartManager.ItemAdded(sampleItem))

      manualTime.timePasses(2 seconds)

      cartManager ! CartManager.RemoveItem(sampleItem.id, sampleItem.count, orderManager.ref)
      orderManager.expectNoMessage()

      cartManager ! CartManager.AddItem(sampleItem, orderManager.ref)
      orderManager.expectMessage(CartManager.ItemAdded(sampleItem))

      simulateSystemCrashAndRestart()

      manualTime.timePasses(2 seconds)

      cartManager ! CartManager.RemoveItem(sampleItem.id, sampleItem.count, orderManager.ref)
      orderManager.expectNoMessage()

      cartManager ! CartManager.AddItem(sampleItem, orderManager.ref)
      orderManager.expectMessage(CartManager.ItemAdded(sampleItem))

      simulateSystemCrashAndRestart()

      cartManager ! CartManager.RemoveItem(sampleItem.id, sampleItem.count, orderManager.ref)
      orderManager.expectMessage(CartManager.ItemRemoved(sampleItem.id, sampleItem.count))
    }

    def simulateSystemCrashAndRestart(): Unit = {
      testKit.shutdownTestKit()

      testKit = ActorTestKit(ActorTestKitBase.testNameFromCallStack())
      orderManager = testKit.createTestProbe[CartManager.CartEvent]()
      manualTime = ManualTime()(testKit.system)
      val latch = new CountDownLatch(1)
      cartManager = testKit.spawn(CartManager.behavior("CartManager", Some(latch)))
      latch.await()
    }
  }
}

object CartManagerTest {
  val sampleItem = Item(URI.create("1"), "Product", "Brand", 1, 1)
}
